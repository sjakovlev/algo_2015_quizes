package ee.ut.cs.aa.quizes.quiz_1A;

import java.util.Arrays;

public class Solution {

    public static int task1(int n, int steps[]) {
        if (n < 0) {
            // No solutions exist for a negative number of steps.
            return 0;
        }

        if (n == 0) {
            // Created a complete solution - print it and return 1.
            System.out.println(Arrays.toString(steps));
            return 1;
        }

        // Still have to make additional steps - continue recursively.

        int solutionCount = 0;

        int stepsAndOne[] = Arrays.copyOf(steps, steps.length + 1);
        stepsAndOne[steps.length] = 1;
        solutionCount += task1(n - 1, stepsAndOne);

        if (n > 1) {
            int stepsAndTwo[] = Arrays.copyOf(steps, steps.length + 1);
            stepsAndTwo[steps.length] = 2;
            solutionCount += task1(n - 2, stepsAndTwo);
        }

        return solutionCount;
    }

    public static boolean task2a(int values[], int counts[], int s) {
        if (values.length != counts.length) {
            throw new IllegalArgumentException("Lengths of value and count arrays must be the same.");
        }

        if (s < 0) {
            // No legal solutions exist for a negative sum.
            return false;
        }

        if (s == 0) {
            // Found legal solution.
            return true;
        }

        // Continue to recursively search for solutions.

        for (int i = 0; i < values.length; i++) {
            if (values[i] <= s && counts[i] > 0) {
                int newCounts[] = Arrays.copyOf(counts, counts.length);
                newCounts[i] -= 1;
                if (task2a(values, newCounts, s - values[i])) {
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean task2b(int values[], int s, int currentPosition) {
        if (s < 0) {
            return false;
        }

        if (s == 0) {
            return true;
        }

        if (currentPosition < 0 || currentPosition >= values.length) {
            return false;
        }

        int currentValue = values[currentPosition];

        for (int i = 0; i <= 3; i++) {
            if (task2b(values, s - currentValue * i, currentPosition + 1)) {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        int answer1 = task1(3, new int[]{});
        System.out.println("Task one: " + answer1);

        boolean answer2a = task2a(new int[]{5, 10, 25, 100}, new int[]{3, 3, 3, 3}, 400);
        System.out.println("Task two (a): " + answer2a);

        boolean answer2b = task2b(new int[]{5, 10, 25, 100}, 400, 0);
        System.out.println("Task two (b): " + answer2b);
    }
}
