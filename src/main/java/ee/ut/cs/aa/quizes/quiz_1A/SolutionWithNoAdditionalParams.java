package ee.ut.cs.aa.quizes.quiz_1A;

import java.util.Arrays;

/**
 * This solution is intended as a Proof of Concept that you can solve given tasks with no additional arguments
 * except those that are described in the tasks themselves. However, this is not a practical way to solve these tasks.
 */
public class SolutionWithNoAdditionalParams {

    public static int task1(int n) {
        int res[] = new int[1];

        Runnable r = new Runnable() {
            @Override
            public void run() {
                res[0] = task1(n, new int[]{});
            }

            public int task1(int n, int steps[]) {
                if (n < 0) {
                    // No solutions exist for a negative number of steps.
                    return 0;
                }

                if (n == 0) {
                    // Created a complete solution - print it and return 1.
                    System.out.println(Arrays.toString(steps));
                    return 1;
                }

                // Still have to make additional steps - continue recursively.

                int solutionCount = 0;

                int stepsAndOne[] = Arrays.copyOf(steps, steps.length + 1);
                stepsAndOne[steps.length] = 1;
                solutionCount += task1(n - 1, stepsAndOne);

                if (n > 1) {
                    int stepsAndTwo[] = Arrays.copyOf(steps, steps.length + 1);
                    stepsAndTwo[steps.length] = 2;
                    solutionCount += task1(n - 2, stepsAndTwo);
                }

                return solutionCount;
            }
        };

        r.run();
        return res[0];
    }

    public static boolean task2(int values[], int s) {
        boolean res[] = new boolean[1];
        int counts[] = new int[values.length];
        Arrays.fill(counts, 3);

        Runnable r = new Runnable() {
            @Override
            public void run() {
                res[0] = task2(values, counts, s);
            }

            public boolean task2(int values[], int counts[], int sum) {
                if (values.length != counts.length) {
                    throw new IllegalArgumentException("Lengths of value and count arrays must be the same.");
                }

                if (sum < 0) {
                    // No legal solutions exist for a negative sum.
                    return false;
                }

                if (sum == 0) {
                    // Found legal solution.
                    return true;
                }

                // Continue to recursively search for solutions.

                for (int i = 0; i < values.length; i++) {
                    if (values[i] <= sum && counts[i] > 0) {
                        int newCounts[] = Arrays.copyOf(counts, counts.length);
                        newCounts[i] -= 1;
                        if (task2(values, newCounts, sum - values[i])) {
                            return true;
                        }
                    }
                }

                return false;
            }
        };

        r.run();
        return res[0];
    }

    public static void main(String[] args) {
        int answer1 = task1(3);
        System.out.println("Task one: " + answer1);

        boolean answer2 = task2(new int[]{5, 10, 25, 100}, 425);
        System.out.println("Task two: " + answer2);
    }
}
