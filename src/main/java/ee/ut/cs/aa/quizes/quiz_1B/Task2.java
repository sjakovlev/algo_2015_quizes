package ee.ut.cs.aa.quizes.quiz_1B;

public class Task2 {

    /**
     * Check if if there is a set of items that has at most {@code canTake} elements
     * and which has a weight equal to {@code weightNeeded} among items in array {@code weights}
     * beginning from the item after {@code beginAt}.
     *
     * @param weightNeeded how much weight is still missing (need to add this much)
     * @param canTake how many items we can still add
     * @param weights array of item weights
     * @param beginAt index of an item in the {@code weights} array; all items before it will be ignored
     * @return true if there is at least one valid set that fits the conditions
     */
    public static boolean task2(int weightNeeded, int canTake, int weights[], int beginAt) {

        if (beginAt < 0) throw new IllegalArgumentException("beginAt cannot be negative"); // sanity check

        if (weightNeeded == 0) return true; // found valid combination

        if (weightNeeded < 0) return false; // cannot decrease weight by taking more items

        if (canTake <= 0) return false; // cannot take more items

        for (int i = beginAt; i < weights.length; i++) {
            if (task2(weightNeeded - weights[i], canTake - 1, weights, i + 1)) {
                System.out.println(weights[i]); // for debugging - not needed
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        int data[] ={1, 2, 1, 1, 1, 3, 4, 2};
        System.out.println(task2(10, 13, data, 0) ? "SOLUTION FOUND" : "NO SOLUTION");
    }
}
